# include "basic_matrix.hh"
# include <iostream>

int main(int argc, char const *argv[])
{
    basic_matrix<int> m1 = {{2,-3},{1,4}}, m2 = {{-2},{5}}, m3 = {{-1,6},{-2,3}}, m4 = {{-7},{-2}};

    cout << (m1*m2)-(m3*m4) << endl;

    return 0;
}