# include <utility>
# include <initializer_list>
# include <cmath>
# include <iostream>

using namespace std;

template<typename type>
class basic_matrix
{
private:
  pair<unsigned, unsigned> size;
  type ** ptr;
  type valid;

public:
  basic_matrix()
    : size{0,0}, ptr{nullptr}, valid{}
  {
    //Empty
  }

  basic_matrix(basic_matrix<type> & rm)
  {
    this->operator=(rm);
  }

  basic_matrix(basic_matrix<type> && mm)
  {
    this->operator=(mm);
  }  

  basic_matrix(pair<unsigned, unsigned> p)
    : size{p}
  {
    this->instance(this->size);
  }

  basic_matrix(unsigned fst, unsigned scnd)
    : size{make_pair(fst,scnd)}
  {
    this->instance(this->size);
  }

  basic_matrix(initializer_list<initializer_list<type>> ls)
  {
    this->size.first = ls.size();

    unsigned scnd = 0, i, j;
    
    for(auto item : ls)
      if (item.size() > scnd)
        scnd = item.size();
    
    this->size.second = scnd;

    this->instance(this->size);

    i = 0;
    j = 0;

    for(auto item : ls)
    {
      for(auto val : item)
      {
        this->ptr[i][j] = val;
        j++;
      }
      i++;
      j = 0;
    }
  }

  ~basic_matrix()
  {
     this->clean();	
  }

  void instance(pair<unsigned, unsigned> sz)
  {
    this->ptr = new type*[sz.first];

    for (unsigned i = 0; i < sz.first; ++i)
      this->ptr[i] = new type[sz.second];
  }

  void instance(unsigned fst, unsigned scnd)
  {
    this->ptr = new type*[fst];

    for (unsigned i = 0; i < fst; ++i)
      this->ptr[i] = new type[scnd];
  }

  void clean()
  {
    for (unsigned i = 0; i < this->size.first; ++i)
      delete[] ptr[i];

    delete[] ptr;
  }

  pair<unsigned,unsigned> get_size()
  {
    return this->size;
  }

  type * operator[](unsigned i)
  {
    return this->ptr[i];
  }

  basic_matrix & operator=(basic_matrix & m)
  {
    if(this->size.first != 0 or this->size.second != 0)
      this->clean();

    this->size = m.get_size();

    this->instance(this->size);

    for (unsigned i = 0; i < this->size.first; ++i)
      for (unsigned j = 0;j < this->size.second;++j)
        this->ptr[i][j] = m[i][j];

    return *this;
  }

  basic_matrix & operator=(basic_matrix && m)
  {
    if(this->size.first != 0 or this->size.second != 0)
      this->clean();

    this->size = m.get_size();

    this->instance(this->size);

    for (unsigned i = 0; i < this->size.first; ++i)
      for (unsigned j = 0;j < this->size.second;++j)
        this->ptr[i][j] = m[i][j];

    return *this;
  }

  bool is_empty()
  {
    if (this->size.first == 0 and this->size.second == 0)
      return true;
    return false;
  }
};

template<typename type>
bool operator==(basic_matrix<type> & m1, basic_matrix<type> & m2)
{
  if (m1.get_size() != m2.get_size())
    return false;

  bool flag = true;

  for (unsigned i = 0; i < m1.get_size().first; ++i)
  {
    for (unsigned j = 0; j < m2.get_size().second; ++j)
    {
      if (m1[i][j] != m2[i][j])
      {
        flag = false;
        break;
      }
    }
  }

  return flag;
}

template<typename type>
basic_matrix<type> operator*(unsigned k, basic_matrix<type> & m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(long unsigned k, basic_matrix<type> & m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(long long unsigned k, basic_matrix<type> & m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(int k, basic_matrix<type> & m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i = 0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;
  return result;
}

template<typename type>
basic_matrix<type> operator*(long k, basic_matrix<type> & m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(long long k, basic_matrix<type> & m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(unsigned k, basic_matrix<type> && m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(long unsigned k, basic_matrix<type> && m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(long long unsigned k, basic_matrix<type> && m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(int k, basic_matrix<type> && m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i = 0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;
  return result;
}

template<typename type>
basic_matrix<type> operator*(long k, basic_matrix<type> && m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(long long k, basic_matrix<type> && m)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> & m, unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> & m, long unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> & m,long long unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> & m, int k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i = 0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;
  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> & m, long k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> & m, long long k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> && m, unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> && m, long unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> && m, long long unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> && m, int k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i = 0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;
  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> && m, long k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> && m, long long k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m.get_size());

  for (unsigned i =   0; i < m.get_size().first; ++i)
    for (unsigned j = 0; j < m.get_size().second; ++j)
      result[i][j] = m[i][j] * k;

  return result;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> & m, unsigned k)
{
  m = m * k;
  return m;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> & m, long unsigned k)
{
  m = m * k;
  return m;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> & m, long long unsigned k)
{
  m = m * k;
  return m;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> & m, int k)
{
  m = m * k;
  return m;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> & m, long k)
{
  m = m * k;
  return m;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> & m, long long k)
{
  m = m * k;
  return m;
}

template<typename type>
basic_matrix<type> operator+(basic_matrix<type> & m1, basic_matrix<type> & m2) 
{
  if((m1.get_size() != m2.get_size()) or (m1.is_empty() or m2.is_empty()))
    return basic_matrix<type>();

  basic_matrix<type> result(m1.get_size().first, m2.get_size().second);
  
  for (unsigned i = 0; i < result.get_size().first; ++i)
    for (unsigned j = 0; j < result.get_size().second; ++j)
      result[i][j] = m1[i][j] + m2[i][j];
  
  return result;
}

template<typename type>
basic_matrix<type> operator+(basic_matrix<type> && m1, 
                             basic_matrix<type> && m2) 
{
  if((m1.get_size() != m2.get_size()) or (m1.is_empty() or m2.is_empty()))
    return basic_matrix<type>();

  basic_matrix<type> result(m1.get_size().first, m2.get_size().second);
  
  for (unsigned i = 0; i < result.get_size().first; ++i)
    for (unsigned j = 0; j < result.get_size().second; ++j)
      result[i][j] = m1[i][j] + m2[i][j];
  
  return result;
}

template<typename type>
basic_matrix<type> operator-(basic_matrix<type> & m1, basic_matrix<type> & m2)
{
  m2 *= -1;
  return ((m2)+(m1));
}

template<typename type>
basic_matrix<type> operator-(basic_matrix<type> && m1, 
                             basic_matrix<type> && m2)
{
  m2 *= -1;
  return ((m2)+(m1));
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> & m1, basic_matrix<type> & m2) 
{
  if(m1.get_size().second != m2.get_size().first 
    or (m1.is_empty() and m2.is_empty()))
      return basic_matrix<type>();

  basic_matrix<type> result(m1.get_size().first, m2.get_size().first);

  for (unsigned i = 0; i < result.get_size().first; ++i)
    for (unsigned j = 0; j < result.get_size().second; ++j)
      for (unsigned k = 0; k < m1.get_size().second; ++k)
        result[i][j] += m1[i][k] * m2[k][j];
  
  return result;
}

template<typename type>
basic_matrix<type> operator*(basic_matrix<type> && m1, basic_matrix<type> && m2) 
{
  if(m1.get_size().second != m2.get_size().first 
    or (m1.is_empty() and m2.is_empty()))
      return basic_matrix<type>();

  basic_matrix<type> result(m1.get_size().first, m2.get_size().first);
  
  for (unsigned i = 0; i < result.get_size().first; ++i)
    for (unsigned j = 0; j < result.get_size().second; ++j)
      for (unsigned k = 0; k < m1.get_size().second; ++k)
        result[i][j] += m1[i][k] * m2[k][j];
  
  return result;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> & m1, basic_matrix<type> & m2)
{
  m1 = m1*m2;
  return m1;
}

template<typename type>
basic_matrix<type> & operator*=(basic_matrix<type> && m1, basic_matrix<type> && m2)
{
  m1 = m1*m2;
  return m1;
}
/*<!--------Still under construcion! --------------!>*/
template<typename type>
basic_matrix<type> pow(basic_matrix<type> & m, unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m);

  for (unsigned i = 0; i < k; ++i)
    result = result * result;

  return result;
}

template<typename type>
basic_matrix<type> pow(basic_matrix<type> && m, unsigned k)
{
  if (m.is_empty())
    return basic_matrix<type>();

  basic_matrix<type> result(m);

  for (unsigned i = 0; i < k; ++i)
    result *= result;

  return result;
}
/*<!--------Still under construcion! --------------!>*/
template<typename type>
ostream & operator<<(ostream & os, basic_matrix<type> & m)
{
  if (m.is_empty())
    return (os << "{[]}");

  unsigned i, j;

  os << '{';
  for (i = 0; i < m.get_size().first;i++)
  {
    os << '[';
    for (j = 0;j < m.get_size().second-1;j++)
      os << m[i][j] << ',';
    os << m[i][j];
    if (i != m.get_size().first-1)
      os << "],";
  }
  os << "]}";

  return os;
}

template<typename type>
ostream & operator<<(ostream & os, basic_matrix<type> && m)
{
  if (m.is_empty())
    return (os << "{[]}");

  unsigned i, j;

  os << '{';
  for (i = 0; i < m.get_size().first;i++)
  {
    os << '[';
    for (j = 0;j < m.get_size().second-1;j++)
      os << m[i][j] << ',';
    os << m[i][j];
    if (i != m.get_size().first-1)
      os << "],";
  }
  os << "]}";

  return os;
}

template<typename type>
basic_matrix<type> transpose(basic_matrix<type> & m)
{
  basic_matrix<type> r(m.get_size().second, m.get_size().first);

  for (unsigned i = 0; i < r.get_size().first; ++i)
    for (unsigned j = 0; j < r.get_size().second; ++j)
      r[i][j] = m[j][i];

  return r;
}

template<typename type>
basic_matrix<type> transpose(basic_matrix<type> && m)
{
  basic_matrix<type> r(m.get_size().second, m.get_size().first);

  for (unsigned i = 0; i < r.get_size().first; ++i)
    for (unsigned j = 0; j < r.get_size().second; ++j)
      r[i][j] = m[j][i];

  return r;
}
